from django.shortcuts import render
from art.models import Painting

def index(request):
    return render(request, 'art/index.html', {'paintings': Painting.objects.all()})